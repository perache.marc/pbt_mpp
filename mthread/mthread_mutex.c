#include <errno.h>
#include "mthread_internal.h"



  /* Functions for mutex handling.  */

  /* Initialize MUTEX using attributes in *MUTEX_ATTR, or use the
     default values if later is NULL.  */
int
mthread_mutex_init (mthread_mutex_t * __mutex,
		    const mthread_mutexattr_t * __mutex_attr)
{
	if(__mutex != NULL) /*Si le pointeur est valide*/
	{
		/*On applique toujours la valeurs par défaut, peu importe la valeurs de __mutex_attr*/
		__mutex->nb_thread=0;
		__mutex->lock=0;	/*0 == mutex unlock, autre == lock*/
		/*Initialisation de la liste*/
		__mutex->list = (mthread_list_t *) malloc(sizeof (mthread_list_t));
		__mutex->list->first=NULL;
		__mutex->list->last=NULL;
		__mutex->list->lock=0;
	}
  return 0;
}

  /* Destroy MUTEX.  */
int
mthread_mutex_destroy (mthread_mutex_t * __mutex)
{
	if(__mutex->nb_thread == 0) /*Mutex unlock*/
	{
		/*On libère*/
		free(__mutex->list);
		/*Et c'est tout*/
	}
	else /*Mutex lock*/
	{
		/*Erreur*/
		errno=EBUSY;
		return -1;
	}
  return 0;
}

  /* Try to lock MUTEX.  */
int
mthread_mutex_trylock (mthread_mutex_t * __mutex)
{
	if(__mutex == NULL || __mutex->list == NULL) /*Erreur*/
	{
		errno=EINVAL;
		return -1;
	}
	mthread_spinlock_lock(& __mutex->lock);
	if(__mutex->nb_thread == 0) /*Le mutex n'est pas lock*/
	{
		mthread_spinlock_unlock(& __mutex->lock);
	}
	else
	{
		errno=EBUSY;
		mthread_spinlock_unlock(& __mutex->lock);
		return -1;
	}
  return 0;
}

  /* Wait until lock for MUTEX becomes available and lock it.  */
int
mthread_mutex_lock (mthread_mutex_t * __mutex)
{
	if(__mutex == NULL || __mutex->list == NULL) /*Erreur*/
	{
		errno=EINVAL;
		return -1;
	}
	mthread_spinlock_lock(& __mutex->lock);
	if(__mutex->nb_thread == 0) /*Le mutex n'est pas lock*/
	{
		/*On le lock*/
		__mutex->nb_thread=1;
		mthread_spinlock_unlock(& __mutex->lock);
	}
	else
	{
		mthread_self()->status = BLOCKED;
		mthread_insert_last(mthread_self(), __mutex->list);

		/*On enregistre notre spinlock*/
		mthread_virtual_processor_t *vp = mthread_get_vp();
		vp->p = & __mutex->lock;

		mthread_yield(); /*On ce yield. C'est le sched qui retire le spinlock*/
	}
  return 0;
}

  /* Unlock MUTEX.  */
int
mthread_mutex_unlock (mthread_mutex_t * __mutex)
{
	if(__mutex == NULL || __mutex->list == NULL) /*Erreur*/
	{
		errno=EINVAL;
		return -1;
	}
	mthread_spinlock_lock(& __mutex->lock);
	if(__mutex->list->first == NULL) /*La liste est vide*/
	{
		__mutex->nb_thread=0;
	}
	else
	{
		mthread_t next;
		next = mthread_remove_first(__mutex->list);
		next->status=RUNNING;
	}
	mthread_spinlock_unlock(& __mutex->lock);
  return 0;
}
